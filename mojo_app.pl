#!/usr/bin/env perl
use strict;
use warnings;
use Mojolicious::Lite;
use Mojo::Log;
use Data::Printer;
use JSON::XS;
use FindBin;
use lib "$FindBin::Bin/lib";
#use Persistance::MongoDB;
#use Persistance::Hash;
#use Persistance::EventStreamHash;
use Persistance::EventStreamMongoDB;
use GetPostById;
use GetNPosts;
use AddPost;
use EditPost;


#my $persistance_gateway = Persistance::EventStreamHash->new();
#my $persistance_gateway = Persistance::Hash->new();
my $persistance_gateway = Persistance::EventStreamMongoDB->new(

    {
        host => "mongo",
        port => 27017,
        db_name => 'blog',
        username => "admin",
        password => 'admin',
    }
);
my $get_by_id = GetPostById->new({posts_gateway => $persistance_gateway});
my $get_n_posts = GetNPosts->new({posts_gateway => $persistance_gateway});
my $add_post = AddPost->new({posts_gateway => $persistance_gateway});
my $edit_post = EditPost->new({posts_gateway => $persistance_gateway});

my $log = Mojo::Log->new;

get '/' => sub {
    my $c = shift;
    $c->render(template => "index");
};

get "/:all" => sub {
    my $c = shift;
    $c->redirect_to("/");
};

get "/rest/post/:id" => sub {
    my $c = shift;
    my $id = $c->param("id");
    $log->info("Get id: $id");
    my ($post) = $get_by_id->execute($id);
    $c->render(json => {title => $post->title, content => $post->content, create_date => $post->create_date});
};

get "/rest/posts/:nr" => sub {
    my $c = shift;
    my $nr = $c->param("nr") - 1;

    $log->info("Getting $nr posts");

    my $posts = $get_n_posts->execute($nr);
    # $c->render(json => {post_ids => \@{$posts}});
    $c->render(json => \@{$posts});

};

post "/rest/post" => sub {
    my $c = shift;
    my $body = decode_json $c->req->body;

    my ($id, $err) = $add_post->execute($body);
    $log->info("Added post, id: $id");
    $c->render(json => {post_id => $id});
};

put "/rest/post/:id" => sub {
    my $c = shift;
    my $post_id = $c->param("id");
    my $body = decode_json $c->req->body;
    my ($id, $err) = $edit_post->execute($post_id, $body);
    if (not $err) {
        $log->info("Editing post, id: $id");
        $c->render(json => {post_id => $id});
    } else {
        $log->warn("post not found: $post_id");
        $c->render(json => {error => "post id not found: $post_id"})
    }
};

app->start;

# Dev:
# docker run --rm -v `pwd`:/code -p 3000:3000 blog-app:dev

__DATA__

@@index.html.ep
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>The evolving blog</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.13.3/react.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.13.3/JSXTransformer.js"></script>
        <!-- <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script> Later maybe -->
        <script type="text/jsx" src="js/blog.js"></script>
    </head>
    <body>
        <div class="container">

            <nav>
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo">Blog</a>
                </div>
            </nav>

            <div id="blog"></div>
        </div>
    </body>
</html>

