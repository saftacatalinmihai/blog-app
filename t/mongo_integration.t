#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Printer;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Persistance::MongoDB;

my $posts_gateway = Persistance::MongoDB->new(
    {
        host => "ds055722.mongolab.com",
        port => 55722,
        db_name => 'blog',
        username => "admin",
        password => 'admin',
    }
);
# p $posts_gateway;
# my ($id, $err) = $posts_gateway->add_post("Title1", "Content4");
# $posts_gateway->add_post("Title2", "Content4");
# $posts_gateway->add_post("Title3", "Content4");
# $posts_gateway->add_post("Title4", "Content4");

# my $p = $posts_gateway->get_by_id('55c8d17752fcf6009a16be31');
# p $p;

my $posts = $posts_gateway->get_n_posts(4);
p $posts;


# my $p1 = $posts_gateway->get_by_id(1);
# ok($p1->title eq "Post 1");

# my $p2 = $posts_gateway->get_by_id(2);
# ok($p2->title eq "Post 2");

# my $p3 = $posts_gateway->get_by_id(3);
# ok($p3->title eq "None");

# done_testing();