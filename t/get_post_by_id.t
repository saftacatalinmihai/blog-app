#!/usr/bin/perl -w

use strict;
use warnings;
use Test::More;
use Data::Printer;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Persistance::Hash;

my $posts_gateway = Persistance::Hash->new();

my $p1 = $posts_gateway->get_by_id(1);
ok($p1->title eq "Post 1");

my $p2 = $posts_gateway->get_by_id(2);
ok($p2->title eq "Post 2");

my $p3 = $posts_gateway->get_by_id(3);
ok($p3->title eq "None");

done_testing();