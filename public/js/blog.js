var Post = React.createClass({ displayName: "Post",
	render: function() {
		return (
		    <div className="row">
                <div className="col s12">
                  <div className="card blue-grey darken-1">
                    <div className="card-content white-text">
                      <span className="card-title">{this.props.title}</span>
                      <p>{this.props.content}</p>
                      <br></br>
                      <p>{this.props.create_date}</p>
                    </div>
                  </div>
                </div>
            </div>
		);
	}
});

var PostList = React.createClass({displayName: "PostList",
	render: function() {
		var postNodes = this.props.data.map(function (post) {
			return (
				<Post title={post.title} content={post.content} create_date={post.create_date}></Post> 
			)
		});
		return (
			<div className = "PostList">
				{postNodes}
			</div>
		);
	}
});

var CommentBox = React.createClass({displayName: 'CommentBox',
    render: function() {
        return (
          React.createElement('div', {className: "commentBox"},
            "Hello, world! I am a CommentBox."
        	)
    	);
 	}
});
// var data = [
// 	{title: "Post1", content: "Content 1", create_date: "22/08/2015"},
// 	{title: "Post2", content: "Content 2", create_date: "22/08/2015"},
// 	{title: "Post3", content: "Content 3", create_date: "22/08/2015"}
// ];
var BlogBox = React.createClass({
	getInitialState: function() {
		console.log(this.props.url)
	    return {data: []};
	},
	componentDidMount: function() {
	    $.ajax({
	     	url: this.props.url,
	     	dataType: 'json',
	     	cache: false,
	     	success: function(post_ids) {
	     		this_posts = this;
				console.log(post_ids);
				post_ids.map( function(post_id){
					$.ajax({
						url: '/rest/post/' + post_id,
						dataType: 'json',
						cache: false,
						success: function(post_data){
							new_data = this_posts.state.data;
							new_data.push(post_data);
							this_posts.setState({data: new_data});
						},
						error: function(xhr, status, err) {
							console.error(this_posts.props.url, status, err.toString());
						}
					});
				});
	      	}.bind(this),
	      	error: function(xhr, status, err) {	
	        	console.error(this.props.url, status, err.toString());
	      	}.bind(this)
	    });
  	},
	render: function() {
		return (
			<PostList data={this.state.data}/>
		);
	}
});

React.render(
  <BlogBox url="/rest/posts/40" />,
  document.getElementById('blog')
);

