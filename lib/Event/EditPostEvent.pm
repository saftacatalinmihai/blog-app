package Event::EditPostEvent;
use strict;
use warnings FATAL => 'all';
use Data::Printer;
use DateTime;
use Moose;
use Post;

has "id" => ( is => 'ro', isa => 'Str' );
has "title" => ( is => 'ro', isa => 'Str' );
has "content" => ( is => 'ro', isa => "Str" );

sub play {
    my $self = shift;
    my $crt_post = shift;
    return unless $crt_post;
    return $crt_post->clone({title => $self->title, content => $self->content});
}

__PACKAGE__->meta->make_immutable;

1;