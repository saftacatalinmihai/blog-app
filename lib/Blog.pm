package Blog;
use strict;
use warnings;
use Moose;

has 'posts' => (is => 'ro', isa => 'ArrayRef[Post]');

__PACKAGE__->meta->make_immutable;

1;