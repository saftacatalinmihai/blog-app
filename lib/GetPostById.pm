package GetPostById;
use strict;
use warnings;
use Moose;
use Data::Printer;

has 'posts_gateway' => (is=> 'ro', isa => 'PostGateway');

# This could be a place to check for pre and post conditions
#around 'execute' => sub {
#    my $orig = shift;
#    my $self = shift;
#    my $id = shift;
#    p $orig;
#    p $self;
#    p $id;
#    my @ret = $self->$orig($id);
#    p @ret;
#    return $ret[0];
#};

sub execute{
    my ($elf, $id) = @_;
    return $elf->posts_gateway->get_by_id($id);
}

__PACKAGE__->meta->make_immutable;

1;