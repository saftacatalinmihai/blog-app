package Persistance::EventStreamMongoDB;
use strict;
use warnings;
use Data::Printer;
use DateTime;
use Moose;
use MongoDB;
use Post;
use List::Util qw(reduce);
use List::MoreUtils qw(uniq);
use Event::NewPostEvent;
use Event::EditPostEvent;

with 'PostGateway';

has 'host' => (is => "ro", isa => "Str");
has 'port' => (is => 'ro', isa => "Int");
has 'db_name' => (is => 'ro', isa => 'Str');
has 'username' => (is => 'ro', isa => 'Str');
has 'password' => (is => 'ro', isa => 'Str');

has 'client' => ( 
    is => 'ro', 
    isa => 'MongoDB::MongoClient',
    lazy => 1,
    builder => '_build_client',
);

has 'db' => (is => 'ro', lazy => 1, builder => '_build_db');
has 'events_coll' => (is => 'ro', lazy => 1, builder => '_build_events_coll');
has 'materialized_view_coll' => (is => 'ro', lazy => 1, builder => '_build_materialized_view_coll');

sub BUILD {
    my $self = shift;
    $self->_materialize_view();

}

sub _build_client{
    my $self = shift;
    my $conn = MongoDB::MongoClient->new(
        host => $self->host,
        port => $self->port,
    );
    return $conn;
};

sub _build_db{
    my $self = shift;
    return $self->client->get_database($self->db_name);
};

sub _build_events_coll{
    my $self = shift;
    return $self->db->get_collection('events');
};

sub _build_materialized_view_coll{
    my $self = shift;
    return $self->db->get_collection('materialized_posts_view');
};

sub _get_crt_state{
    my ($self, $post_id) = @_;

    return unless $self->event_coll.find({'event_data.id' => $post_id});
    my @post_events = $self->events_coll->find({'event_data.id' => $post_id})->sort({create_date => 1})->all();

    my @post_event_objects = map { sub {
        my $ev = shift;
        if ($ev->{event_type} eq "new_post_event") { return Event::NewPostEvent->new($ev->{event_data})}
        if ($ev->{event_type} eq "edit_post_event") { return Event::EditPostEvent->new($ev->{event_data})}
    }->($_)} @post_events;
    return if not @post_event_objects;
    return reduce { $b->play($a)} $post_event_objects[0]->play(), @post_event_objects;
}

sub _get_all_posts_crt_state{
    my $self = shift;
    my @uniq_ids = uniq map {$_->{event_data}->{id}} $self->events_coll->find({},{_id => 1})->sort({create_date => 1})->all();
    return grep defined map {$self->_get_crt_state($_)} @uniq_ids;
}

sub _materialize_view{
    my $self = shift;
    my @MATERIALIZED_VIEW_POSTS = $self->_get_all_posts_crt_state();
    for my $post (@MATERIALIZED_VIEW_POSTS) {
        $self->materialized_view_coll->update(
            {_id => MongoDB::OID->new(value => $post->{id})},
            {
                title => $post->title,
                content => $post->content,
                create_date => $post->create_date,
            },
            {upsert => 1}
        )
    }
}

sub get_by_id {
    my ( $self, $id ) = @_;
    my $p = $self->materialized_view_coll->find_one({_id => MongoDB::OID->new(value => $id)});
    return Post->new(
        id => $p->{'_id'}->value,
        title => $p->{'title'},
        content => $p->{'content'},
        create_date => $p->{'create_date'} || DateTime->now(),
    );
};

sub get_n_posts {
    my ($self, $nr) = @_;
    my @db_posts = $self->materialized_view_coll->find({},{_id => 1})->sort({create_date => 1})->limit($nr)->all();
    my @ids = map { $_->{'_id'}->value} @db_posts;
    return \@ids;
};

sub add_post{
    my ($self, $data) = @_;
    my $post = Post->new(%{$data});
    my $post_id = MongoDB::OID->new();
    $self->events_coll->insert({
        event_type => "new_post_event",
        event_data => {
            id => $post_id->value(),
            title => $post->title,
            content => $post->content,
            create_date => $post->create_date,
        }
    });
    $self->_materialize_view();
    return ($post_id->value, 0);
};

sub edit_post{
    my ($self, $post_id, $edit) = @_;
    if ($self->event_coll.find({'event_data.id' => $post_id})) {
        $self->events_coll->insert({
            event_type => "edit_post_event",
            event_data => {
                id => $post_id,
                %{$edit}
            }
        });
        $self->_materialize_view();
        return ($post_id, 0);
    } else {
        return (0, "Post not found with id: $post_id");
    }
}

1;
