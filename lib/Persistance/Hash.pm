package Persistance::Hash;
use strict;
use warnings;
use Data::Printer;
use DateTime;
use Moose;
use Post;

with 'PostGateway';

my $POSTS = [
    Post->new( { id => 6, title => "Post 6", content => "Some text" } ),
    Post->new( { id => 5, title => "Post 5", content => "Some text" } ),
    Post->new( { id => 4, title => "Post 4", content => "Some text" } ),
    Post->new( { id => 3, title => "Post 3", content => "Some text" } ),            
    Post->new( { id => 2, title => "Post 2", content => "Some text" } ),
    Post->new( { id => 1, title => "Post 1", content => "Some text" } ),
];

sub get_by_id {
    my $elf = shift;
    my $id  = shift;
    my @p   = grep { $_->id == $id } grep defined, @{$POSTS};
    return $p[0] if $p[0];
    return Post->new( { id => 0, title => 'None', content => "None" } );
};

sub get_n_posts{
    my ($elf, $nr) = @_;
    my @posts = map {$_->id} grep defined, @{$POSTS}[0 .. $nr];

    return \@posts;
};

sub add_post{
    my ($elf, $title, $content) = @_;
    my $last_post_id = $elf->get_n_posts(1)->[0];

    unshift @{$POSTS}, Post->new({
        id => $last_post_id + 1, 
        title => $title,
        content => $content,
    });
    return ( $last_post_id + 1, 0);
};

__PACKAGE__->meta->make_immutable;

1;
