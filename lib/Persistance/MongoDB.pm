package Persistance::MongoDB;
use strict;
use warnings;
use Data::Printer;
use DateTime;
use Moose;
use MongoDB;
use Post;

with 'PostGateway';

has 'host' => (is => "ro", isa => "Str");
has 'port' => (is => 'ro', isa => "Int");
has 'db_name' => (is => 'ro', isa => 'Str');
has 'username' => (is => 'ro', isa => 'Str');
has 'password' => (is => 'ro', isa => 'Str');

has 'client' => ( 
    is => 'ro', 
    isa => 'MongoDB::MongoClient',
    lazy => 1,
    builder => '_build_client',
);

has 'db' => (is => 'ro', lazy => 1, builder => '_build_db');
has 'posts_coll' => (is => 'ro', lazy => 1, builder => '_build_posts_coll');

sub _build_client{
    my $self = shift;
    my $conn = MongoDB::MongoClient->new(
        host => $self->host,
        port => $self->port,
    );

    return $conn;
};

sub _build_db{
    my $self = shift;
    return $self->client->get_database($self->db_name);
};

sub _build_posts_coll{
    my $self = shift;
    return $self->db->get_collection('posts');
};

sub get_by_id { 
    my ( $self, $id ) = @_;
    my $p = $self->posts_coll->find_one({_id => MongoDB::OID->new(value => $id)});
    return Post->new(
        id => $p->{'_id'}->value,
        title => $p->{'title'},
        content => $p->{'content'},
        create_date => $p->{'create_date'} || DateTime->now(),
    );
};

sub get_n_posts {
    my ($self, $nr) = @_;
    my @db_posts = $self->posts_coll->find({},{_id => 1})->sort({create_date => 1})->limit($nr)->all();

    my @ids = map { $_->{'_id'}->value} @db_posts;
    return \@ids;
};

sub add_post{
    my ($self, $title, $content) = @_;
    my $post = Post->new({title => $title, content => $content});
    my $id = $self->posts_coll->insert(
        {title => $post->title, content => $post->content, create_date => $post->create_date}
    );
    return ($id->value, 0);
};

1;
