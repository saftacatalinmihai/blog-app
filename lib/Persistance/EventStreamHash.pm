package Persistance::EventStreamHash;
use strict;
use warnings FATAL => 'all';
use Data::Printer;
use DateTime;
use Moose;
use Event::NewPostEvent;
use Event::EditPostEvent;
use List::Util qw(reduce);
use List::MoreUtils qw(uniq);

with 'PostGateway';

my $EVENTS = [
    {event_type => "edit_post_event", event_data => { id => 5, content => "Some other text"}},
    {event_type => "new_post_event", event_data => { id => 6, title => "Post 6", content => "Some text"}},
    {event_type => "new_post_event", event_data => { id => 5, title => "Post 5", content => "Some text"}},
    {event_type => "edit_post_event", event_data => { id => 3, content => "Some other new text"}},
    {event_type => "edit_post_event", event_data => { id => 3, content => "Some other text"}},
    {event_type => "new_post_event", event_data => { id => 4, title => "Post 4", content => "Some text"}},
    {event_type => "new_post_event", event_data => { id => 3, title => "Post 3", content => "Some text"}},
    {event_type => "new_post_event", event_data => { id => 2, title => "Post 2", content => "Some text"}},
    {event_type => "new_post_event", event_data => { id => 1, title => "Post 1", content => "Some text"}},
];

_materialize_view();
my $MATERIALIZED_VIEW_POSTS;
sub _materialize_view{
    my @MATERIALIZED_VIEW_POSTS = _get_all_posts_crt_state($EVENTS);
    $MATERIALIZED_VIEW_POSTS = \@MATERIALIZED_VIEW_POSTS;
}

sub _get_crt_state{
    my ($post_id, $events) = @_;
    my @post_event_objects = map { sub {
        my $ev = shift;
        if ($ev->{event_type} eq "new_post_event") { return Event::NewPostEvent->new($ev->{event_data})}
        if ($ev->{event_type} eq "edit_post_event") { return Event::EditPostEvent->new($ev->{event_data})}
    }->($_)} grep {$_->{event_data}->{id} == $post_id} reverse @{$events};
    return reduce { $b->play($a)} $post_event_objects[0]->play(), @post_event_objects;
}

sub _get_all_posts_crt_state{
    my $events = shift;
    my @uniq_ids = uniq map {$_->{event_data}->{id}} @{$events};
    return map {_get_crt_state($_, $events)} @uniq_ids;
}

sub get_by_id {
    my $elf = shift;
    my $id  = shift;
    my @p   = grep { $_->id == $id } grep defined, @{$MATERIALIZED_VIEW_POSTS};
    return $p[0] if $p[0];
    return Post::none_post();
};

sub get_n_posts{
    my ($elf, $nr) = @_;
    my @posts = map {$_->id} grep defined, @{$MATERIALIZED_VIEW_POSTS}[0 .. $nr];

    return \@posts;
};

sub add_post{
    my ($elf, $data) = @_;
    my $last_post_id = $elf->get_n_posts(1)->[0];
    my $new_post_event = {
        event_type => "new_post_event",
        event_data => {
            id => $last_post_id + 1,
            %{$data}
        }
    };
    unshift @{$EVENTS}, $new_post_event;
    _materialize_view();
    return ( $last_post_id + 1, 0);
};

sub edit_post{
    my ($self, $post_id, $edit) = @_;
    my $edit_event = {
        event_type => "edit_post_event",
        event_data => {
            id => $post_id,
            %{$edit}
        }
    };
    unshift @{$EVENTS}, $edit_event;
    _materialize_view();
    return ($post_id, 0);
}

__PACKAGE__->meta->make_immutable;

1;