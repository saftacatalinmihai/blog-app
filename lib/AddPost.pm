package AddPost;
use strict;
use warnings;
use Moose;
use Data::Printer;

has 'posts_gateway' => (is=> 'ro', isa => 'PostGateway');

#before 'execute' => sub {
#    p @_;
#};

sub execute{
    my ($elf, $data) = @_;
    return $elf->posts_gateway->add_post($data);
}

__PACKAGE__->meta->make_immutable;

1;