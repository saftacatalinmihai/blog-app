package EditPost;
use strict;
use warnings;
use Moose;

has 'posts_gateway' => (is=> 'ro', isa => 'PostGateway');

sub execute{
    my ($elf, $post_id, $edit) = @_;
    return $elf->posts_gateway->edit_post($post_id, $edit);
}

__PACKAGE__->meta->make_immutable;

1;