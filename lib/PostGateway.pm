package PostGateway;
use strict;
use warnings;
use Data::Printer;
use Moose::Role;

requires 'get_by_id', 'get_n_posts', 'add_post', 'edit_post';

1;

