package GetNPosts;
use strict;
use warnings;
use Moose;

has 'posts_gateway' => (is=> 'ro', isa => 'PostGateway');

sub execute{
    my ($elf, $nr) = @_;
    return $elf->posts_gateway->get_n_posts($nr);
}

__PACKAGE__->meta->make_immutable;

1;