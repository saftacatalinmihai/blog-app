package Post;
use strict;
use warnings;
use Moose;
use DateTime;
use utf8;

has "id" => ( is => 'ro', isa => 'Str' );
has "title" => ( is => 'ro', isa => 'Str' );
has "content" => ( is => 'ro', isa => "Str" );
has "create_date" => ( is => 'ro', isa => 'DateTime', builder => '_build_create_date' );

sub _build_create_date{
    my $elf = shift;
    return DateTime->now;
}

sub clone {
    my $self = shift;
    my $new_args = shift;
    return Post->new({
        id => $self->id,
        title => defined $new_args->{title} ? $new_args->{title} : $self->title,
        content => defined $new_args->{content} ? $new_args->{content} : $self->title,
    });
}

sub none_post {
    return Post->new( { id => 0, title => 'None', content => "None" } )
}


__PACKAGE__->meta->make_immutable;

1;