FROM perl:5.20

RUN cpanm install Moose \
 && cpanm install Mojolicious \
 && cpanm install Data::Printer \
 && cpanm install DateTime \
 && cpanm install JSON::XS \
 && cpanm install MongoDB

COPY . /usr/src/blog_app
WORKDIR /usr/src/blog_app

EXPOSE 3000

CMD ["perl", "./mojo_app.pl", "daemon"]
